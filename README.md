# bigDataV
> 大屏数据可视化 big screen data visualization demo

## 地图数据可视化 - 基于ECharts Geo

地图热点、飞线动效，世界地图、中国地图、省份地图、城市地图

## 3D图表展示 - 基于ECharts GL

3D柱形图，3D地球，二维数据的3D化展示

## 阿里云DataV示例 - 企业实时销售大盘

基于DataV行业模板快速生成，采用静态JSON数据

## 百度Sugar示例 - 深交所上市公司全景概览

以已有的大屏为模板快速创建，切换风格，更改数据源

演示地址：[http://shen-yu.gitee.io/bigdatav/](http://shen-yu.gitee.io/bigdatav/)